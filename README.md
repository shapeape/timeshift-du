# timeshift du

Command line tool to display disc usage (`du`) of timeshift snapshots together with their **tags**.

It displays the snapshot name (the date and time it was taken), the size and the tags of the snapshot:  

**H**: hourly  
**D**: daily  
**W**: weekly   
**M**: monthly  
**B**: boot  
**O**: ondemand  


The Program will ask for your sudo password as it is required by `du`  and `mount`.  
Execution takes a while depending on your snapshots size.

Example output:
<pre>
<b>Snapshot                  Size      Tags</b>
----------------------------------------
 2020-08-28_11-00-01      9,1G        M
 2020-09-28_11-00-01      9,5G      W M
 2020-10-05_12-00-02      1,3G        W
 2020-10-12_12-00-02      5,4G        W
 2020-10-22_11-00-01      1,4G      D W
 2020-10-23_11-00-01       53G        D
 2020-10-26_10-00-01      757M        D
 2020-10-27_11-00-01      1,9G        D
 2020-10-28_11-00-01      1,2G      D M
-----------------------------------------
<b>Total size                 83G</b>              
</pre>

**Not tested with BTRFS!**