#!/usr/bin/env python3

"""
Display disk usage of timeshift snapshots together with their tags (boot, daily, weekly... )
"""
import json
import os
import subprocess
import time

path = '/run/timeshift/backup'
snapshot_path = '/timeshift/snapshots'
timeshift_tags = ['-ondemand', '-boot', '-hourly', '-daily', '-weekly', '-monthly']


def get_uuid():
    """
    read backup_device_uid from timeshift settings
    :return: backup_device_uuid
    """
    settings_file = '/etc/timeshift/timeshift.json'
    with open(settings_file, 'r') as file:
        data = file.read()
    obj = json.loads(data)
    # print("backup_device_uuid: " + str(obj['backup_device_uuid']))
    return str(obj['backup_device_uuid'])


def mount(m_disc, m_path):
    """
    mount path if not mounted
    :param m_disc: disc to mount
    :param m_path: path to mount to
    :return: none
    """
    if not os.path.ismount(m_path):
        if not os.path.isdir(m_path):
            os.system('sudo mkdir -p ' + m_path)
        os.system('sudo -S mount ' + m_disc + ' ' + m_path)


def du():
    """
    execute du on the path
    :return: list of lines with output of du
    """
    return subprocess.check_output(['sudo', '-S', 'du', '-hd1', path + snapshot_path]).splitlines()


def get_tags(snapshot):
    """
    returns timeshift-tags for a given snapshot
    :param snapshot: name (date) of the snapshot
    :return: string of space separated tags (D W W...)
    """
    snapshot_tags = ''
    for tag in timeshift_tags:
        if os.path.islink(path + snapshot_path + tag + '/' + snapshot):
            snapshot_tags += tag[1].capitalize() + ' '
    return snapshot_tags


def sort_dict(to_sort):
    """
    sort dictionary by name of keys
    :param to_sort: dictionary to sort
    :return: sorted dictionary
    """
    return {key: val for key, val in sorted(to_sort.items(), key=lambda ele: ele[0])}


def get_time_human(t):
    """

    :param t:
    :return:
    """
    result = ''
    d = t // 86400
    t -= d * 86400
    h = t // 3600
    t -= h * 3600
    m = t // 60
    t -= m * 60
    s = t
    if d:
        result += f'{int(d)} days, '
    if h:
        result += f'{int(h)} hours, '
    if m:
        result += f'{int(m)} minutes, '
    result += f'{s:.1f} seconds'
    return result


def print_formatted(lines):
    """
    format lines and print them
    :param lines: lines to format
    :return: none
    """
    # print bold and normal
    bold = '\033[1m'
    normal = '\033[0m'

    # ignore first line as it is the total
    output = {}
    for line in lines[:-1]:
        size = line.split()[0].decode('utf-8')
        snapshot = line.split()[1].decode('utf-8').replace(path + snapshot_path, '').replace('/', '')
        tags = get_tags(snapshot)
        output.update({snapshot: [size, tags]})
    output = sort_dict(output)

    # formatted line with minimum width and alignment per column
    print(f"\n{bold}{'Snapshot': <20}{'Size': >10} {'Tags': >10}{normal}")
    # dashed line
    print('-' * 40)
    for key in output.keys():
        size = output[key][0]
        tags = output[key][1]
        print(f"{key: >20}{size: >10}{tags: >10}")

    # summary
    total_size = lines[-1].split()[0].decode("utf-8")
    print('-' * 40)
    print(f"{bold}{'Total size': <20}{total_size: >10}{normal}")


def timeshift_is_running():
    """
    checks if timeshift is currently doing a snapshot
    :return: true or false
    """
    is_running = False
    for subdir in os.listdir('/proc'):
        if subdir == 'timeshift':
            is_running = True
            break
    return is_running


def main():
    if timeshift_is_running():
        print('timeshift is currently doing a backup, please try later')
        exit()
    else:
        mount('UUID=' + get_uuid(), path)

        t_start = time.time()
        print_formatted(du())
        t_end = time.time()
        time_total = t_end - t_start
        print(f'\ntimeshift du took {get_time_human(time_total)}')


if __name__ == '__main__':
    main()
